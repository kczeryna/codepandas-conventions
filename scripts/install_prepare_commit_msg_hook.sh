#!/bin/bash

HOOKS_DIR=$(git rev-parse --show-toplevel)/.git/hooks
HOOK_FILE=${HOOKS_DIR}/prepare-commit-msg
SCRIPT_URL="https://gitlab.com/NetworkedAssets/codepandas-conventions/raw/master/scripts/prepare-commit-msg"

printf -- " - Delete previous git hook...\n"
rm -f "$HOOK_FILE"

printf -- " - Downloading git hook...\n"
curl -s "$SCRIPT_URL" > "$HOOK_FILE"

printf -- " - Making git hook executable...\n"
chmod a+x "$HOOK_FILE"