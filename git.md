# Naming conventions for branches and commits

## TODO
- decide what to do with branches related to multiple tickets?
- improve the script, and not prepend the commit msg when it already contains `[ticket_id]`

## Branches
Each branch, related to a ticket in issue tracking system (e.g. jira) should have following format

`{feature,bugfix}/<ticket_id>/description`

examples:
- `feature/RAD-42/some-awesome-feature`
- `bugfix/RAD-420/sad-bugfix`

If current branch is not related to any ticket, branch name should contain only description.

## Commits
Each commit message made on branch related to `feature` or `bugfix` should be prepended with `[ticket_id]`
(note the square braces).

example: `[RAD-123] added svelte`

**Note** commit message prefix can be added automatically via git hook.
Just run following command somewhere in the repo:

`sh <(curl -s https://gitlab.com/NetworkedAssets/codepandas-conventions/raw/master/scripts/install_prepare_commit_msg_hook.sh)`

this will download and install git commit hook.