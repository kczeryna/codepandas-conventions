Set of conventions used in CodePandas team.
- [git naming conventions](git.md) - naming convention for branches and commit messages
- [code review conventions](code_review.md) - based on Google's guidelines
- [kotlin coding conventions](kotlin/coding_conventions.md) - based on official JetBrains document