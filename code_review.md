## Overview
Code review conventions in CodePandas team follow guidelines follow Google's best practices document located
[here](https://google.github.io/eng-practices/review/)

The most controversial, but in my opinion crucial rule of Code Review would be:

> In general, reviewers should favor approving a CL once it is in a state where it definitely improves the overall 
> code health of the system being worked on, even if the CL isn’t perfect.

Please also note, that this document introduces a way of describing "nitpicks":

> prefix it with something like “Nit: “ to let the author know that it’s just a point of polish that they could choose to ignore.

Instead of `Nit:` please use `Nitpick:` as it's already established way of expressing such "nice to have" changes.

**Note** `CL` in this documentation means `Change List` - list of changes to be reviewed.

## Relevant guidelines
Although the whole document contains valuable information, only following paragraphs apply to our day to day work: 

- [The Standard of Code Review](https://google.github.io/eng-practices/review/reviewer/standard.html)
- [What to Look For In a Code Review](https://google.github.io/eng-practices/review/reviewer/looking-for.html)
- [How to Write Code Review Comments](https://google.github.io/eng-practices/review/reviewer/comments.html)
- [Small CLs](https://google.github.io/eng-practices/review/developer/small-cls.html)
- [How to Handle Reviewer Comments](https://google.github.io/eng-practices/review/developer/handling-comments.html)

